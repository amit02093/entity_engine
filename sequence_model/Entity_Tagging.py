
# coding: utf-8

# In[56]:



# Now we will create 
import marshal 
import pandas as pd
import nltk
from gensim.models import Word2Vec
import os
import random

import numpy as np


# In[96]:


# starting with cluster 0
# we found the cluster 2 - rooms
# cluster 4 - location
# cluster 5 - names
# cluster 6 - food 

def annotation(file_path,data,list_entities,number_of_clusters):
    
    for i in range(number_of_clusters):
        marshal_file = os.path.join(marshal_path,'cluster_'+str(i)+'_words.marshal')
        marshal_in = open(marshal_file,'rb')
        cluster = marshal.load(marshal_in)
        list_cluster_words.append(cluster)
    dict_clusters =dict(zip(list_entities,list_cluster_words))
    # Feedback contains list of reviews of the whole corpus
    
    
    for feedback in data:
        print('Performing annotation .....')
        for sent in nltk.sent_tokenize(feedback):
            for word in nltk.word_tokenize(sent):
                for tag , list_words in dict_clusters.items():
                    if(word in list_words and tag != 'O'):
                        word = word + ' '+tag
                    else:
                        word = word +' O'
                    
                list_tagged_feedbacks.append(word)
    
    random.shuffle(list_tagged_feedbacks)
    textfile = open(file_path,'w')
    textfile.writelines(word+'\n'+'\n' if word == '. O' else word+'\n' for word in list_tagged_feedbacks)
    del list_tagged_feedbacks[:]
    print('Writing done!!')

        


# In[94]:


# Loading the Dataframe containing whole corpus
df = pd.read_pickle('/home/ubuntu/Amit_workspace/Entity_model/Exeter-POC1/Pickles/Dataset_pickle',compression = 'bz2')
feedbacks = df['Content'].tolist()

marshal_path = '/home/ubuntu/Amit_workspace/Entity_model/Exeter-POC1/marshal_files'
train_file_path = '/home/ubuntu/Amit_workspace/Entity_model/Exeter-POC1/Model_train/sequence_tagging/data/train.txt'
dev_file_path = '/home/ubuntu/Amit_workspace/Entity_model/Exeter-POC1/Model_train/sequence_tagging/data/dev.txt'
test_file_path = '/home/ubuntu/Amit_workspace/Entity_model/Exeter-POC1/Model_train/sequence_tagging/data/test.txt'

#list_entities = ['O','O','P-PER','O','L-LOC','F-FOOD','R-ROOMS']

# list to hold words for each of the cluster
list_cluster_words = []
#list to holf the tagged feedbacks 
list_tagged_feedbacks = []
# shuffling and splitting dataset into 3 parts
train, dev, test = np.split(df['Content'].sample(frac=1), [int(.95*len(df)), int(.99*len(df))])
number_of_clusters = 7


# In[97]:


'Perform Annotations'

def Annotate(entities):
    
    #Train
    #train_data = train.tolist()
    #annotation(train_file_path,train_data,list_entities,number_of_clusters)

    #Dev
    #dev_data = dev.tolist()
    #annotation(dev_file_path,dev_data,list_entities,number_of_clusters)

    #Test
    list_entities = entities
    test_data = test.tolist()
    annotation(dev_file_path,test_data,list_entities,number_of_clusters)


if __name__ == "__main__":
    Annotate(entities)